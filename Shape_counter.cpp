#include <opencv2\highgui\highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
    int three = 0;
    int rectangle = 0;
    int square = 0;

    Mat img = imread("C:/images/blob5.jpg");
    Mat gray;
    cvtColor(img, gray, COLOR_BGR2GRAY);

    Mat thresh;
    threshold(gray, thresh, 50, 255, 0);

    vector<vector<Point> > contours;
    findContours(thresh, contours,  RETR_LIST, CHAIN_APPROX_SIMPLE);
    //cout << "Number of contours detected: " << contours.size() << endl;
    

    for (int i = 0; i < contours.size(); i++)
    {
        Point2f p1 = contours[i][0];
        vector<Point> approx;
        approxPolyDP(contours[i], approx, arcLength(contours[i], true) * 0.01, true);
        
        if (approx.size() == 3)
        {
            three = three + 1;
        }
        if (approx.size() == 4)
        {
            Rect r = boundingRect(contours[i]);
            float ratio = float(r.width) / r.height;
            
            
            if (ratio >= 0.9 && ratio <= 1.1)
            {
                square = square + 1;
            }
            else
            {
                rectangle = rectangle + 1;
            }
        }
    }
    // Printed in debug console
    cout << "Found triangles" << three << endl;
    cout << "Found squares  " << square << endl;
    cout << "Found  rectangles " << rectangle << endl;
    
    
    waitKey(0);
    destroyAllWindows();

    
}

