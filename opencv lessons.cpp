#include<iostream>
#include <highlevelmonitorconfigurationapi.h>
#include <opencv2\opencv.hpp>
#include <opencv2\highgui\highgui.hpp>





using namespace cv;
using namespace std;

// This is Hue points for specific color
Scalar yellowLow = Scalar(25, 130, 180);
Scalar yellowHigh = Scalar(45, 255, 255);
Scalar greenLow = Scalar(46, 40, 40);
Scalar greenHigh = Scalar(70, 255, 255);
Scalar blueLow = Scalar(100, 150, 150);
Scalar blueHigh = Scalar(140, 255, 255);
Scalar redLow = Scalar(170, 140, 160);
Scalar redHigh = Scalar(180, 255, 255);


int main(int argc, char** argv)
{


    // Reading the image file from the location
    //creating a matrix
    Mat img = imread("C:/images/blob6.jpg");

    

    //if image is not present
    if (img.empty()) {
        cout << "Cannot open the image or image is not present" << endl;
        return 0;
        
    }
    
    Mat imgcopy;
    img.copyTo(imgcopy);

    cvtColor(img, img, COLOR_BGR2HSV);
    
    Mat mask;
    inRange(img, greenLow, greenHigh, mask);

    vector < vector < Point>> contours;
    findContours(mask, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    drawContours(imgcopy, contours, -1, (0, 255, 0), 3);

     	

    imshow("mask", mask);
    imshow("Drawcontours", imgcopy);
    
    waitKey(0);
    destroyAllWindows();

}



