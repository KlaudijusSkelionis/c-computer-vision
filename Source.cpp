#include <opencv2\highgui\highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>


using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
   

    Mat img = imread("C:/images/blob5.jpg");
    Mat gray;
    cvtColor(img, gray, COLOR_BGR2GRAY);

    Mat thresh;
    threshold(gray, thresh, 50, 255, 0);

    vector<vector<Point> > contours;
    findContours(thresh, contours,  RETR_LIST, CHAIN_APPROX_SIMPLE);
    cout << "Number of contours detected: " << contours.size() << endl;
    

    for (int i = 0; i < contours.size(); i++)
    {
        Point2f p1 = contours[i][0];
        vector<Point> approx;
        approxPolyDP(contours[i], approx, arcLength(contours[i], true) * 0.01, true);
        
        if (approx.size() == 3)
        {
            
            drawContours(img, contours, i, Scalar(200, 0, 240), 3);
            putText(img, "Triangle", p1, FONT_HERSHEY_SIMPLEX, 0.6, Scalar(255, 0, 255), 2);
        }
        if (approx.size() == 4)
        {
            Rect r = boundingRect(contours[i]);
            float ratio = float(r.width) / r.height;
            
            
            if (ratio >= 0.9 && ratio <= 1.1)
            {
               
                drawContours(img, contours, i, Scalar(0, 0, 255), 3);
                putText(img, "Square", p1, FONT_HERSHEY_SIMPLEX, 0.6, Scalar(255, 255, 0), 2);
            }
            else
            {
                
                drawContours(img, contours, i, Scalar(102, 51, 0), 3);
                putText(img, "Rectangle", p1, FONT_HERSHEY_SIMPLEX, 0.6, Scalar(102, 51, 0), 2);
            }
        }
    }
    
    imshow("Shapes", img);
    
    
    waitKey(0);
    destroyAllWindows();

    
}

